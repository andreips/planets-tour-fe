import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {PlanetComponent} from './components/planet/planet.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {RobotsPipe} from "./utils/RobotsPipe";
import {StoreModule} from "@ngrx/store";
import {storeReducer} from "./store/store.reducer";
import {UpdatePlanetComponent} from './components/update-planet/update-planet.component';
import {MaterialModule} from "./material.module";
import {NotificationComponent} from './components/notification/notification.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AuthService} from "./services/auth.service";
import {AuthGuardService} from "./services/auth-guard.service";
import {LoginComponent} from './components/login/login.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {PlanetsService} from "./services/planets.service";

@NgModule({
  declarations: [
    AppComponent,
    PlanetComponent,
    DashboardComponent,
    RobotsPipe,
    UpdatePlanetComponent,
    NotificationComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    StoreModule.forRoot({store: storeReducer}),
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    AuthService,
    AuthGuardService,
    PlanetsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
