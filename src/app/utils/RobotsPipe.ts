import {Pipe, PipeTransform} from "@angular/core";
import {Robot} from "../models/Robot";

@Pipe({ name: 'robotslist' })
export class RobotsPipe implements PipeTransform {
    transform(robots: Robot[]): any {
        let robotslist = "";
        robots.forEach((robot, index) => {
            if (index > 2) {
                robotslist += " ...";
                return robotslist;
            }
            if (index != 0) {
                robotslist += ", ";
            }
            robotslist += robot.name;
        });
        return robotslist;
    }
}
