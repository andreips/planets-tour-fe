export class Utils {
  static IsNullOrUndefined(value) {
    return value === null || value === undefined;
  }
}
