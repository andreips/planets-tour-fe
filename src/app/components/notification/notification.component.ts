import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from "@angular/material/snack-bar";
import {select, Store} from "@ngrx/store";

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  constructor(private store: Store, private _snackBar: MatSnackBar){ }

  ngOnInit(): void {
    this.store.select(store => store).subscribe((data: any) => {
      if (data.store.notification) {
        this._snackBar.open(data.store.message, "Close", {
          duration: 3000,
          panelClass: data.store.panelClass
        });
      }
    });
  }
}
