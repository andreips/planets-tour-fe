import { Component, OnInit } from '@angular/core';
import {Planet} from "../../models/Planet";
import {Store} from "@ngrx/store";
import {setPlanets, triggerNotification} from "../../store/store.actions";
import {ActivatedRoute, Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";
import {PlanetsService} from "../../services/planets.service";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public planets: Planet[];
  public loaded = false;

  constructor (private store: Store, private router: Router, private snackBar: MatSnackBar, private route: ActivatedRoute, private planetsService: PlanetsService) { }

  ngOnInit(): void {
      this.store.select(state => state).subscribe((data: any) => {
        if (data.store.planets.length > 0) {
            this.planets = data.store.planets;
            this.loaded = true;
        } else {
            this.getPlanets();
        }
      });
  }

  redirectToPlanet (id: number): void {
    this.router.navigate(['/planet', id]);
  }

  async getPlanets () {
    this.planetsService.getAll().then((response) => {
      this.planets = response;
      this.store.dispatch(setPlanets({ planets: this.planets }));
    })
    .finally(() => {
      this.loaded = true;
    });
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }
}
