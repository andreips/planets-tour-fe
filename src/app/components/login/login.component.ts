import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {User} from "../../models/User";
import {AuthService} from "../../services/auth.service";
import {Utils} from "../../utils/Utils";
import {triggerNotification} from "../../store/store.actions";
import {Store} from "@ngrx/store";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  usernameFormControl = new FormControl(null, [Validators.required]);
  passwordFormControl = new FormControl('', Validators.required);

  constructor (private store: Store, private formBuilder: FormBuilder, private router: Router, private authService: AuthService) { }

  ngOnInit (): void {
    if (localStorage.getItem('token') != null) {
      this.router.navigateByUrl('/dashboard');
    }
    this.loginForm = this.formBuilder.group({
      username: this.usernameFormControl,
      password: this.passwordFormControl
    });
  }

  login () {
    const userCredentials = this.loginForm.value;
    const user = new User(userCredentials.username, userCredentials.password);
    this.authService.login(user).subscribe(
      response => {
        if (!Utils.IsNullOrUndefined(response) && !Utils.IsNullOrUndefined(response.token)) {
          this.authService.setToken(response.token);
          this.router.navigate(['/dashboard']);
          this.store.dispatch(triggerNotification({ message: "Login succesfully!", panelClass: "success" }));
        } else {
          this.store.dispatch(triggerNotification({ message: response, panelClass: "error" }));
        }
      },
      error => {
        this.store.dispatch(triggerNotification({ message: "Login failed!", panelClass: error.error.Message }));
      });
  }
}
