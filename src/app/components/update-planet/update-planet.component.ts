import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {Planet} from "../../models/Planet";
import {ActivatedRoute, Router} from "@angular/router";
import {setPlanets, triggerNotification} from "../../store/store.actions";
import {CONSTANTS} from "../../constants/constants";
import {Status} from "../../models/Status";
import {DomSanitizer} from "@angular/platform-browser";
import {PlanetsService} from "../../services/planets.service";
import {Utils} from "../../utils/Utils";

@Component({
  selector: 'app-update-planet',
  templateUrl: './update-planet.component.html',
  styleUrls: ['./update-planet.component.css']
})
export class UpdatePlanetComponent implements OnInit {
  public planet: Planet;
  public statuses: Status[] = [
    new Status(1, "TODO", "#ffcc00"),
    new Status(2, "OK", "#23fa5c"),
    new Status(3, "!OK", "#A0515D"),
    new Status(4, "En route", "#d6d6d6"),
  ];
  private planets: Planet[];

  constructor (private sanitizer: DomSanitizer, private store: Store, private route: ActivatedRoute, private router: Router, private planetsService: PlanetsService) { }

  ngOnInit(): void {
    this.statuses = CONSTANTS.PLANET_STATUSES;
    this.route.params.subscribe(params => {
        if (isNaN(params['id'])) {
          this.triggerError();
        } else {
          this.store.select(store => store).subscribe((data: any) => {
              this.planets = [...data.store.planets];
              const planet = this.planets.find(x => x.id === +params['id']);
              if (planet) {
                this.planet = { ...planet };
              } else {
                if (!data.store.notification) {
                  this.triggerError();
                }
              }
          });
        }
    });
  }

  triggerError () {
    this.store.dispatch(triggerNotification({ message: "This planet does not exist!", panelClass: "error" }));
    this.backToDashboard();
  }

  backToDashboard () {
    this.router.navigate(['dashboard']);
  }

  savePlanet () {
    this.planetsService.updatePlanet(this.planet)
      .then((response) => {
        this.store.dispatch(triggerNotification({ message: "Planet updated!", panelClass: "success" }));
        if (!Utils.IsNullOrUndefined(response.id)) {
          const index = this.planets.findIndex(x => x.id === response.id);
          if (index > -1) {
            this.planets[index] = response;
            this.store.dispatch(setPlanets({ planets: this.planets }));
          }
        }
        this.backToDashboard();
      })
      .catch((error) => {
        this.store.dispatch(triggerNotification({ message: "Server error!", panelClass: "error" }));
      })
  }

  transform (avatar: string) {
    return this.sanitizer.bypassSecurityTrustUrl(avatar);
  }
}
