import {Component, Input, OnInit} from '@angular/core';
import {Status} from "../../models/Status";
import {CONSTANTS} from "../../constants/constants";
import {Planet} from "../../models/Planet";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-planet',
  templateUrl: './planet.component.html',
  styleUrls: ['./planet.component.css']
})
export class PlanetComponent implements OnInit {
  @Input() planet: Planet;
  public planetStatus: Status;

  constructor (private sanitizer: DomSanitizer) {

  }

  ngOnInit (): void {
      this.planetStatus = CONSTANTS.PLANET_STATUSES.find(x => x.id === this.planet.status);
  }

  transform (avatar: string) {
    return this.sanitizer.bypassSecurityTrustUrl(avatar);
  }
}
