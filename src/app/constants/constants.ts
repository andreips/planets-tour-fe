import {Status} from "../models/Status";

export const CONSTANTS = {
    SERVER_URL: "https://localhost:44383/",
    PLANET_STATUSES: [
        new Status(0, "TODO", "#ffcc00"),
        new Status(1, "OK", "#23fa5c"),
        new Status(2, "!OK", "#A0515D"),
        new Status(3, "En route", "#d6d6d6"),
    ]
};
