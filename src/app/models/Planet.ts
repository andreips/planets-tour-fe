import {Team} from "./Team";

export class Planet {
    id: number;
  name: string;
    avatar: string;
    description: string;
    team: Team;
    status: number;
    constructor (id: number, name: string, avatar: string, description: string, team: Team, status: number) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.description = description;
        this.team = team;
        this.status = status;
    }
}
