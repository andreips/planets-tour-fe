import {Robot} from "./Robot";

export class Team {
    id: number;
    captainName: string;
    robots: Robot[];
    constructor (id: number, captainName: string, robots: Robot[]) {
      this.id = id;
      this.captainName = captainName;
      this.robots = robots;
    }
}
