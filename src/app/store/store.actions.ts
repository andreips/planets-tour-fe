import {createAction, props} from "@ngrx/store";
import {Planet} from "../models/Planet";
import {User} from "../models/User";

export const triggerNotification = createAction('TriggerNotification', props<{message: string, panelClass: string}>());
export const turnOffNotification = createAction('TurnOffNotification');
export const setPlanets = createAction('SetPlanets', props<{planets: Planet[]}>());
export const setUser = createAction('SetUser', props<{user: User}>());
export const updatePlanet = createAction('UpdatePlanet', props<{planet: Planet}>());
