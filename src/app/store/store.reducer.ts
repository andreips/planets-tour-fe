import {createReducer, on} from "@ngrx/store";
import {setPlanets, setUser, triggerNotification, turnOffNotification, updatePlanet} from "./store.actions";

export const initialState = {
  planets: [],
  notification: false,
  message: null,
  user: null
}

const _storeReducer = createReducer(initialState,
  on(setUser, (state, {user}) => ({...state, user: user})),
  on(setPlanets, (state, { planets }) => ({ ...state, planets: planets})),
  on(triggerNotification, (state, { message, panelClass }) => ({ ...state, message: message, panelClass: panelClass, notification: true})),
  on(turnOffNotification, (state) => ({...state, notification: false}))
);

export function storeReducer(state, action) {
    return _storeReducer(state, action);
}
