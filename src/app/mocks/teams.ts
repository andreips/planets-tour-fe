import {Team} from "../models/Team";
import * as robots from "./robots";

export const team1 = new Team(1, "Johnathan Smartson", [robots.robot1, robots.robot2, robots.robot3]);
export const team2 = new Team(1, "Hannah Intellis", [robots.robot1]);
export const team3 = new Team(1, "Eva Brains", [robots.robot1, robots.robot2, robots.robot3, robots.robot4]);
