import {Robot} from "../models/Robot";

export const robot1 = new Robot(1, "T2011");
export const robot2 = new Robot(2, "T315");
export const robot3 = new Robot(3, "T72");
export const robot4 = new Robot(4, "T21");
export const robot5 = new Robot(5, "T88");
export const robot6 = new Robot(6, "T33");
