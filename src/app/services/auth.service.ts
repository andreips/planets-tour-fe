import { Injectable } from '@angular/core';
import {Utils} from "../utils/Utils";
import {HttpClient} from "@angular/common/http";
import {User} from "../models/User";
import {CONSTANTS} from "../constants/constants";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient, private router: Router) {}

  public login(userCredentials: User) {
    return this.http.post<any>(CONSTANTS.SERVER_URL + 'login', userCredentials);
  }

  public logout() {
    localStorage.removeItem('token');
    this.router.navigateByUrl('/login');
  }

  public setToken(token): void {
    localStorage.setItem('token', token);
  }

  public deleteToken(): void {
    localStorage.clear();
  }

  public isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
    return !Utils.IsNullOrUndefined(token);
  }

}
