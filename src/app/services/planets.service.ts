import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {CONSTANTS} from "../constants/constants";
import {Planet} from "../models/Planet";

@Injectable({
  providedIn: 'root'
})
export class PlanetsService {
  private headers: HttpHeaders;

  constructor(private http: HttpClient) {}

  public async getAll() {
    this.getHeaders();
    return await this.http.get<any>(CONSTANTS.SERVER_URL + "planets", {headers: this.headers}).toPromise();
  }

  public updatePlanet(planet: Planet) {
    this.getHeaders();
    return this.http.put<any>(CONSTANTS.SERVER_URL + "planets/update", planet, {headers: this.headers}).toPromise();
  }

  private getHeaders() {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      Authorization: 'Bearer ' + localStorage.getItem('token')
    });
  }

}
